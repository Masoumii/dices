<?php

/* Superclass */
abstract class Dice {
  
		/* Globale variabelenn */
		protected $randomNumber = null;
		protected $diceNumbers =  array(0,1, 2, 3, 4, 5, 6);

		/* Constructor */
		public function __construct(){
		}

		/* geeft de waarde terug van het random getal dat gegooid is */
		protected function setNumbers(){
			$this->input = $this->diceNumbers;
			$this->rand_keys = array_rand($this->input,2);
			$this->randomNumber = $this->rand_keys[1];
		}

		protected function getNumbers(){
			$this->setNumbers();
			return $this->randomNumber;
		}
	
	  abstract function roll();
}



class DiceNormal extends Dice {
  
  /* Globale Variabelen */
	protected $randomNumber = null;
  protected $one = "icons/delapouite/dice/svg/dice-six-faces-one.svg";
  protected $two = "icons/delapouite/dice/svg/dice-six-faces-two.svg";
  protected $three = "icons/delapouite/dice/svg/dice-six-faces-three.svg";
  protected $four = "icons/delapouite/dice/svg/dice-six-faces-four.svg";
  protected $five = "icons/delapouite/dice/svg/dice-six-faces-five.svg";
  protected $six = "icons/delapouite/dice/svg/dice-six-faces-six.svg";
  
  /* Constructor */
  public function __construct(){}
   
  /* maakt een random getal van 1 tot 6 en toont deze als dobbelsteen */
  public function roll(){
	
		$this->getNumbers();

		switch ($this->randomNumber) {

		case 1:
			 $this->randomNumber = $this->one;
			break;
		case 2:
			 $this->randomNumber = $this->two;
			break;
		case 3:
			 $this->randomNumber = $this->three;
			break;
		case 4:
			 $this->randomNumber = $this->four;
			break;
		case 5:
			 $this->randomNumber = $this->five;
			break;
		case 6:
			 $this->randomNumber = $this->six;
			break;
	
		default:
				echo "error";
			break;  
					}
		
		return '<img width="100px" src="'.$this->randomNumber.'">';

	}
}

class DiceInverted extends Dice {
  
  /* Globale Variabelen */
	protected $randomNumber = null;
  protected $one =   "icons/skoll/originals/svg/inverted-dice-1.svg";
  protected $two = 	 "icons/skoll/originals/svg/inverted-dice-2.svg";
  protected $three = "icons/skoll/originals/svg/inverted-dice-3.svg";
  protected $four =  "icons/skoll/originals/svg/inverted-dice-4.svg";
  protected $five =  "icons/skoll/originals/svg/inverted-dice-5.svg";
  protected $six =   "icons/skoll/originals/svg/inverted-dice-6.svg";
  
  /* Constructor */
  public function __construct(){}
   
  /* maakt een random getal van 1 tot 6 en toont deze als dobbelsteen */
  public function roll(){
		
		// Haal een random getal op uit de dobbelsteen array ( 1 t/m 6)
		$this->getNumbers();

		switch ($this->randomNumber) {

		case '1':
			 $this->randomNumber = $this->one;
			break;
		case '2':
			 $this->randomNumber = $this->two;
			break;
		case '3':
			 $this->randomNumber = $this->three;
			break;
		case '4':
			 $this->randomNumber = $this->four;
			break;
		case '5':
			 $this->randomNumber = $this->five;
			break;
		case '6':
			 $this->randomNumber = $this->six;
			break;
	
		default:
				echo "error";
			break;  
					}
		
		return '<img width="100px" src="'.$this->randomNumber.'">';

	}
}



 class Dice3DNumbers extends Dice {
  
  /* Globale Variabelen */
	protected $randomNumber = null;
  protected $one =   "icons/delapouite/dice/svg/perspective-dice-one.svg";
  protected $two = 	 "icons/delapouite/dice/svg/perspective-dice-two.svg";
  protected $three = "icons/delapouite/dice/svg/perspective-dice-three.svg";
  protected $four =  "icons/delapouite/dice/svg/perspective-dice-four.svg";
  protected $five =  "icons/delapouite/dice/svg/perspective-dice-five.svg";
  protected $six =   "icons/delapouite/dice/svg/perspective-dice-six.svg";
  
  /* Constructor */
  public function __construct(){}
   
  /* maakt een random getal van 1 tot 6 en toont deze als dobbelsteen */
  public function roll(){
		
		// Haal een random getal op uit de dobbelsteen array ( 1 t/m 6)
		$this->getNumbers();

		switch ($this->randomNumber) {

		case '1':
			 $this->randomNumber = $this->one;
			break;
		case '2':
			 $this->randomNumber = $this->two;
			break;
		case '3':
			 $this->randomNumber = $this->three;
			break;
		case '4':
			 $this->randomNumber = $this->four;
			break;
		case '5':
			 $this->randomNumber = $this->five;
			break;
		case '6':
			 $this->randomNumber = $this->six;
			break;
	
		default:
				echo "error";
			break;  
					}
		
		return '<img width="100px" src="'.$this->randomNumber.'">';

	}
}

 class Dice3DEyes extends Dice {
  
  /* Globale Variabelen */
	protected $randomNumber = null;
  protected $one =   "icons/delapouite/dice/svg/perspective-dice-six-faces-one.svg";
  protected $two = 	 "icons/delapouite/dice/svg/perspective-dice-six-faces-two.svg";
  protected $three = "icons/delapouite/dice/svg/perspective-dice-six-faces-three.svg";
  protected $four =  "icons/delapouite/dice/svg/perspective-dice-six-faces-four.svg";
  protected $five =  "icons/delapouite/dice/svg/perspective-dice-six-faces-five.svg";
  protected $six =   "icons/delapouite/dice/svg/perspective-dice-six-faces-six.svg";
  
  /* Constructor */
  public function __construct(){}
   
  /* maakt een random getal van 1 tot 6 en toont deze als dobbelsteen */
  public function roll(){
		
		// Haal een random getal op uit de dobbelsteen array ( 1 t/m 6)
		$this->getNumbers();

		switch ($this->randomNumber) {

		case '1':
			 $this->randomNumber = $this->one;
			break;
		case '2':
			 $this->randomNumber = $this->two;
			break;
		case '3':
			 $this->randomNumber = $this->three;
			break;
		case '4':
			 $this->randomNumber = $this->four;
			break;
		case '5':
			 $this->randomNumber = $this->five;
			break;
		case '6':
			 $this->randomNumber = $this->six;
			break;
	
		default:
				echo "error";
			break;  
					}
		
		return '<img width="100px" src="'.$this->randomNumber.'">';

	}
}
 

?>