<?php

/* Require the required classss */
require('class.dice.php');

/* Object aanmaken */
$oneNormal = new DiceNormal();
$twoNormal = new DiceNormal();
$threeNormal = new DiceNormal();

$oneInverted = new DiceInverted();
$twoInverted = new DiceInverted();
$threeInverted = new DiceInverted();

$one3DN = new Dice3DNumbers();
$two3DN = new Dice3DNumbers();
$three3DN = new Dice3DNumbers();

$one3DE = new Dice3DEyes();
$two3DE = new Dice3DEyes();
$three3DE = new Dice3DEyes();

/* echo output */
// gooi 3 keer dubbelsteen
echo "<b>DiceNormal</b>";
echo "<br>";
echo $oneNormal->roll();
echo $oneNormal->roll();
echo $oneNormal->roll();
echo "<br><br>";
echo "<b>DiceInverted</b>";
echo "<br>";
echo $oneInverted->roll();
echo $oneInverted->roll();
echo $oneInverted->roll();
echo "<br><br>";
echo "<b>Dice3DNumbers<b>";
echo "<br>";
echo $one3DN->roll();
echo $two3DN->roll();
echo $three3DN->roll();
echo "<br><br>";
echo "<b>Dice3DEyes</b>";
echo "<br>";
echo $one3DE->roll();
echo $one3DE->roll();
echo $one3DE->roll();
?>